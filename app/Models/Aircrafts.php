<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aircrafts extends Model
{
    use HasFactory;

    protected $fillable = ['manufacturer', 'model', 'year_manufactured', 'capacity', 'pilot_id', 'created_at', 'updated_at'];

    public function pilot()
    {
        return $this->belongsTo(Pilot::class, 'pilot_id');
    }
}
