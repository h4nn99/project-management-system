<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'system_owner', 'system_pic', 'start_date', 'duration', 'end_date', 'status', 'lead_developer', 'developers', 'development_methodology', 'system_platform', 'deployment_type'
    ];

    // Define any relationships or additional methods here
}
