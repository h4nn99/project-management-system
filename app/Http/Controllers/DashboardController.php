<?php

namespace App\Http\Controllers;
// use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function show(){
        return match (Auth::user()->roleId) {
             ROLE_ADMIN => $this->adminView() ,
             default => view('pages.dashboard.show')
        };
    }
}
