<?php

namespace App\Http\Controllers;

use App\Models\Users;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\Project;

class HomeController extends Controller
{
    public function index()
    {
        if (Auth::id()) {
            $usertype = Auth()->user()->usertype;

            if ($usertype == 'user') {
                return view('dashboard');
            } else if ($usertype == 'manager') {
                $projects = Project::all();
                return view('ProjectManager.project-manager', compact('projects'));
            }
        }
    }

}
