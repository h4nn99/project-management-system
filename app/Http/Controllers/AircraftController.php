<?php

namespace App\Http\Controllers;

use App\Models\Aircrafts;
use Illuminate\Http\Request;

class AircraftController extends Controller
{
    public function index()
    {
        $aircrafts = Aircrafts::all();
        return view('dashboard', compact('aircrafts'));
    }



    public function edit(Aircrafts $aircraft)
    {
        return view('edit', compact('aircraft'));
    }

    public function update(Request $request, Aircrafts $aircraft)
    {
        $request->validate([
            'manufacturer' => 'required',
            'model' => 'required',
            'year_manufactured' => 'required|integer',
            'capacity' => 'required|integer',
        ]);

        $aircraft->update($request->all());

        return redirect()->route('aircraft.index')->with('success', 'Aircraft updated successfully');
    }
}
