<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('system_owner');
            $table->string('system_pic');
            $table->date('start_date');
            $table->integer('duration');
            $table->date('end_date');
            $table->string('status');
            $table->string('lead_developer');
            $table->text('developers');
            $table->string('development_methodology');
            $table->string('system_platform');
            $table->string('deployment_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
