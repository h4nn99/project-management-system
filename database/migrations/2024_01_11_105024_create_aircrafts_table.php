<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('aircrafts', function (Blueprint $table) {
            $table->id();
            $table->string('manufacturer');
            $table->string('model');
            $table->integer('year_manufactured');
            $table->integer('capacity');
            $table->unsignedBigInteger('pilot_id')->nullable();
            $table->timestamps();

            // Ensure the foreign key constraint is set only if 'pilots' table exists
            if (Schema::hasTable('pilots')) {
                $table->foreign('pilot_id')->references('id')->on('pilots');
            }
        });
    }

};
