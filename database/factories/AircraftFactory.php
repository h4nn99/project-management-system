<?php

// database/factories/AircraftFactory.php

// database/factories/AircraftFactory.php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Aircrafts;

class AircraftFactory extends Factory
{
    protected $model = Aircrafts::class;

    public function definition()
    {
        return [
            'manufacturer' => $this->faker->company,
            'model' => $this->faker->word,
            'year_manufactured' => $this->faker->year,
            'capacity' => $this->faker->numberBetween(1, 200),
        ];
    }
}s


