<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Aircrafts;

class AircraftSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Aircrafts::factory(4)->create();
    }
}
