<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Factories\UserFactory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        // Seed the users table
        UserFactory::new()->create([
            'name' => 'farhan',
            'email' => 'hello3@email.com',
            'password' => bcrypt('farhan'), // Hash the password
        ]);

    }
}
