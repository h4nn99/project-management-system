<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pilot;

class PilotSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Pilot::factory(4)->create();
    }
}
