<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Dashboard
        </h2>
    </x-slot>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

    <br>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">

                <h1>Edit Aircraft</h1>

                <form action="{{ route('aircraft.update', $aircraft) }}" method="post">
                    @csrf
                    @method('PATCH')

                    <!-- Aircraft Fields -->
                    <div class="mb-3">
                        <label for="manufacturer" class="form-label">Manufacturer:</label>
                        <input type="text" class="form-control" name="manufacturer" value="{{ $aircraft->manufacturer }}" required>
                    </div>

                    <div class="mb-3">
                        <label for="model" class="form-label">Model:</label>
                        <input type="text" class="form-control" name="model" value="{{ $aircraft->model }}" required>
                    </div>

                    <div class="mb-3">
                        <label for="year_manufactured" class="form-label">Year Manufactured:</label>
                        <input type="text" class="form-control" name="year_manufactured" value="{{ $aircraft->year_manufactured }}" required>
                    </div>

                    <div class="mb-3">
                        <label for="capacity" class="form-label">Capacity:</label>
                        <input type="text" class="form-control" name="capacity" value="{{ $aircraft->capacity }}" required>
                    </div>

                    <!-- Pilot Fields -->
                    <div class="mb-3">
                        <label for="pilot_name" class="form-label">Pilot Name:</label>
                        <input type="text" class="form-control" name="pilot_name" value="{{ $aircraft->pilot ? $aircraft->pilot->name : 'N/A' }}" required>
                    </div>

                    <!-- Repeat similar fields for other pilot attributes (address, phone_number, email, etc.) -->

                    <!-- Repeat similar fields for other attributes -->
                    <button type="submit" class="btn btn-primary">Update Aircraft</button>
                </form>

            </div>
        </div>
    </div>
</x-app-layout>
